import {defineConfig} from 'astro/config';
import {exec} from "child_process";

exec("git log -1 --format=\"%at\"", (error, stdout) => {
    process.env.GIT_COMMIT_DATE = stdout.trim();
});

export default defineConfig({
    integrations: [],
    build: {
        inlineStylesheets: 'never',
        assets: 'assets'
    },
    publicDir: './static',
    outDir: './public'
});