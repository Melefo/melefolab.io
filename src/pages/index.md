---
layout: ../layouts/default.astro
title: Portfolio
subtitle: Hello, World!
icon: /logo.bw.svg
version: v2.0
author: Victor "Melefo" Trencic
description: Portfolio
keywords: Melefo, Portfolio
---

## Introduction

Hello there! <span class="wave">👋🏻</span>

I am <span class="rainbow">Victor Trencic</span>.

Also known as <span class="card">Melefo</span>.

I'm a full-stack developer from Nantes, France.

You can learn more about me and my projects by looking at the following content and pages of the website.

Or if you want to contact me, here is a
button: [Click me! <span class="emoji relative"></span>](mailto:trencic.victor@gmail.com)

## About me

I'm a student looking to learn about all aspects of computer science.

### Experiences

<ul class="tree"><li><pre style="margin: 0;">╭───────────────────────────────────────────────────╮
│ Permanent Contract - Teaching assistant - Epitech │
╰───────────────────────────────────────────────────╯</pre>

* *SEPTEMBER 2023*
* Launching, monitoring and grading student projects in the Master of Sciences program.

</li></ul>

<ul class="tree"><li><pre style="margin: 0;">╭────────────────────────────────────╮
│ Stage Ingénieur fullstack - Ouidou │
╰────────────────────────────────────╯</pre>

* *APRIL 2022 - AUGUST 2022*
* Respond to customer programming requests and follow project management.

</li></ul>

<ul class="tree"><li><pre style="margin: 0;">╭───────────────────────────────────────────────────╮
│ Occasional trainer - ISEG Summer Code Camp Nantes │
╰───────────────────────────────────────────────────╯</pre>

* *JULY 2021 - 2 weeks*
* Introduce first-year marketing students to computer programming.
* Intended for beginners, this Camp introduces volunteer to web programming languages (HTML, CSS and PHP).

</li></ul>

<ul class="tree"><li><pre style="margin: 0;">╭──────────────────────────────────╮
│ Full-stack Internship - INTELLIA │
╰──────────────────────────────────╯</pre>

* *OCTOBER 2020 - DECEMBER 2020*
* Custom software development in VB.NET with ASP.
* Follow-up of projects from creation to maintenance.
* Respond to customer change requests.

</li></ul>

<ul class="tree"><li><pre style="margin: 0;">╭──────────────────────────────────╮
│ Technician Internship - ORDICITY │
╰──────────────────────────────────╯</pre>

* *DECEMBER 2015 - 2 weeks*
* Analyze error messages, unexpected stops and computer malfunctions.
* Identify and restore programs and operating system.
* Apply appropriate repair procedures.
* Disassemble and reassemble computers to change parts.

</li></ul>

### Education

<ul class="tree"><li><p style="margin: 0;"><strong>Epitech</strong></p>

* Expert in Information Technologies, *2019-2024*. GPA 3.72/4

</li></ul>

<ul class="tree"><li><p style="margin: 0;"><strong>UQAR Université du Québec à Rimouski</strong></p>

* Minor in computer Science, *2023*

</li></ul>

<ul class="tree"><li><p style="margin: 0;"><strong>Notre Dame de Toutes Aides</strong></p>

* Science Baccalaureate, *2019*

</li></ul>

### Volunteering

**DevFest Nantes 2019 & 2021**

A technical conference for developers.\
Installation and preparation of rooms.\
Scan passes at reception.

### Competitions

TODO

### Skills

<figure><pre>
100 │                                           ███ Level in %
    │                 
    │  █
    │  █
 80 │  █      █                                      █
    │  █      █      █                               █
    │  █      █      █       █                       █
    │  █      █      █       █       █               █
 60 │  █      █      █       █       █               █
    │  █      █      █       █       █               █       █
    │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
 40 │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
 20 │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
    │  █      █      █       █       █       █       █       █
 0  └──▀──────▀──────▀───────▀───────▀───────▀───────▀───────▀──
       C#    C/C++  HTML   VueJS 3  PHP   MongoDB  Docker   Rust
                   CSS JS   Nuxt
</pre></figure>

### Soft Skills

TODO

### Competencies

- Knowledge of the basics in several languages.
- Operating System Administration.
- Know how to model and design a website.
- Understand algorithmics.
- Adapt when using new tools.
- Understanding the constraints of an IT project.
- Knowledge of technical terms in English and French.
- Know how to use and create a CI/CD.

### Fields of interest

TODO

### Contact

trencic.victor@gmail.com
[]()
[]()
[]()

## Copyright

This design is heavily inspired by
[Oskar Wickström](https://github.com/owickstrom)'s [Monospace Web](https://owickstrom.github.io/the-monospace-web/).

Made with ❤️ by [melefo](https://gitlab.com/melefo) with [Astro](https://astro.build/) | ©
2024 | [Source code](https://gitlab.com/melefo/melefo.gitlab.io)